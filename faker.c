#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

//void printFlag(int argc, char **argv, char *flag) {
void printFlag(char *enc) {
	char *output = malloc(64);
	memset(output, 0, 64);
	//char *enc = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";

	strcpy(output, enc);

	int outlen = strlen(output);
	int start = 0x20;
	int end = 0x7f;
	int offset = end-start;
	int x = 15;
	int y = 29;
	int z = 8;

	// Start
	for (int i = 0; i < outlen; i++) {
		output[i] = ((((output[i]^x)-y)*z) %offset)+start;
	}

	printf("%s\n", output);
}

void thisone() {
	char *enc = "\\PJ\\fC|)L0LTw@Yt@;Twmq0Lw|qw@w2$a@0;w|)@awmLL|Tw|)LwZL2lhhL0k";
	printFlag(enc);
}

void A() {
	char *enc = "\\PJ\\fCaq(Lw|)$Tw$Tw@wb@ELwbY@hk";
	printFlag(enc);
}

void B() {
	char *enc = "\\PJ\\fCTq00;waq|w)L0LwL$|)L0k";
	printFlag(enc);
}

void C() {
	char *enc = "\\PJ\\fChqqZw|0;w2l|wELL(wYqqE$ahk";
	printFlag(enc);
}

void printMenu() {
	puts("\tMenu:");
	puts("1. Flag A");
	puts("2. Flag B");
	puts("3. Flag C");
	puts("4. Exit");
	printf("> ");
}

int main(int argc, char **argv) {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	while (1) {
		printMenu();
		char buf[32];
		read(0, buf, 32);
		int c = atoi(buf);

		switch (c) {
			case 1:
				A(); break;
			case 2:
				B(); break;
			case 3:
				C(); break;
			case 4:
				exit(0); break;
			default:
				puts("Unknown Input"); break;
		}
		puts("");
	}
	
    return 0;
}
