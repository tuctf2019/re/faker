# Faker

Desc: `One of these things is not like the other. Can you uncover the flag?`

Given files:

* faker

Hints:

* Is there anything hidden inside the binary?

Flag: `TUCTF{7h3r35_4lw4y5_m0r3_70_4_b1n4ry_7h4n_m3375_7h3_d3bu663r}`
