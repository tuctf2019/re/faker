#!/usr/bin/python
import sys

# Read data from files
f = open('out', 'r')
f.readline(); f.readline()
out = f.readline().strip()
f.close()
f = open('flag.txt', 'r')
flag = f.readline().strip()
f.close()

keyspace = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
l = len(keyspace)
print l
print len(out)

# Ensure completion of keyspace
notin = ''
for i in range(l):
    c = keyspace[i]
    if c not in out:
        notin += c
if len(notin) > 0:
    print "Not complete!"
    print notin
    #exit(0)

#d = {}
#for i in range(l):
#    a = keyspace[i]
#    b = out[i]
#    d[a] = b

#d = dict(zip(keyspace, out))
d = dict(zip(out, keyspace))
#print keyspace
#print out

for i in flag:
    sys.stdout.write(d[i])
sys.stdout.write('\n')
sys.stdout.flush()

