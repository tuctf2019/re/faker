# Faker Writeup

## Setup

...

## Solution

1. This is a super easy chal:
1. The binary prompts a menu with 3 options all of which print fake flags
1. The fake flags are all to hint at something hidden in the binary
1. The solution is to run this in GDB and manually execute the hidden function `thisone()` which prints the real flag
